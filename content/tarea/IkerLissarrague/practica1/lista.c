#include "lista.h"
#include <stdio.h>
#include <stdlib.h>

void agrega_elemento(struct lista* lista, void* elemento){

	struct nodo* nuevo_nodo=(struct nodo*)malloc(sizeof(struct nodo));
	nuevo_nodo->elemento=elemento;

	if(lista->longitud==0)
		lista->cabeza=nuevo_nodo;
	else{
		struct nodo* nodo=lista->cabeza;
		for (int i = 0; i < lista->longitud-1; ++i)
			nodo=nodo->siguiente;
		nodo->siguiente=nuevo_nodo;
	}
	lista->longitud=lista->longitud+1;

}

void* obten_elemento(struct lista* lista, int n){
	struct nodo* nodo=lista->cabeza;
	void* elem=0;
	for (int i = 0; i<lista->longitud; ++i){
		if(n==i){
			elem=nodo->elemento;
			break;
		}
		nodo=nodo->siguiente;
	}
	return elem;
}

void* elimina_elemento(struct lista* lista, int n){
	void* elem=0;
	struct nodo* nodo=lista->cabeza;
	struct nodo* sig;
	if(n==0){
		lista->cabeza=nodo->siguiente;
		elem=nodo->elemento;
		free(nodo);
		lista->longitud-=1;
	}
	else{
		for (int i = 0; i < lista->longitud; ++i){
			if(n-1==i){
				sig=nodo->siguiente;
				elem=sig->elemento;
				nodo->siguiente=(sig->siguiente);
				free(sig);
				lista->longitud-=1;
				break;
			}
			nodo=nodo->siguiente;
		}
	}
	return elem;
}

void aplica_funcion(struct lista* lista, void (*f)(void*)){
	struct nodo* nodo=lista->cabeza;
	for (int i = 0; i < lista->longitud; ++i){
		f(nodo->elemento);
		nodo=nodo->siguiente;
	}
}