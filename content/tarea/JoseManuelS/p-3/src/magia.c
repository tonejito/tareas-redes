/*
 * Redes de Computadoras 2019-2.
 * Martinez Sanchez Jose Manuel.
 * file.c
 */
#include <stdio.h>
#include <stdlib.h>
#include <magic.h>

int main(int argc, char **argv){
     FILE *f;
     char fileName[80];
     unsigned char *array;
     long fileSize;
     struct magic_set *magic = magic_open(MAGIC_MIME|MAGIC_CHECK);
     magic_load(magic,NULL);


     for (int i = 1; i < argc; ++i){
       if((f=fopen(argv[i], "rb"))==NULL){
           fprintf(stderr, "Archivo desconocido o inexistente.\n");
           exit(1);
       }else{
           fseek (f , 0 , SEEK_END);
           fileSize = ftell (f);
           rewind (f);
           array=(char *)malloc(fileSize+1);
           fread(array, sizeof(char), 4, f);
           char *mimeType = magic_file(magic,argv[i]);
           printf("Numero magico: %02hhx%02hhx%02hhx%02hhx | Tipo de Archivo: %s\n", array[0], array[1], array[2], array[3], mimeType );
           free(array);
       }
     }
 }
/*
 * file.c
 */
