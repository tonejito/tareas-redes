struct nodo {
  void * elemento;
  struct nodo * siguiente;
};

struct lista {
  struct nodo * cabeza;
  int longitud;
};

void agrega_elemento(struct lista*, void*);
void * obten_elemento(struct lista*, int);
void * elimina_elemento(struct lista*, int);
void aplica_funcion(struct lista*, void (*f)(void *));
