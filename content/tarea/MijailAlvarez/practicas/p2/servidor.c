/* Práctica 2 : Sockets (Redes de Computadoras)
   Alvarez Cerrillo Mijail Aron 310020590
   Este programa es el Servidor, el cual recibe un numero como argumento, el
   cual indicara el puerto por el que se pondra en escucha. */
#include<stdio.h>
#include<stdlib.h>
#include<sys/types.h>
/* Para poder usar write and read */
#include<unistd.h>
/* Para poder usar sockets
   https://pubs.opengroup.org/onlinepubs/7908799/xns/syssocket.h.html 
   Sockets allow communication between two different processes on the same or 
   different machines. To be more precise, it's a way to talk to other computers
   using standard Unix file descriptors. In Unix, every I/O action is done by
   writing or reading a file descriptor. A file descriptor is just an integer
   associated with an open file and it can be a network connection, a text 
   file, a terminal, or something else.*/
#include<sys/socket.h>
/* Para poder usar la estrucutra sockaddr_in The <netinet/in.h> header defines
   the sockaddr_in structure that includes at least the following member:
   sa_family_t    sin_family
   in_port_t      sin_port
   struct in_addr sin_addr
   unsigned char  sin_zero[8]
   The sockaddr_in structure is used to store addresses for the Internet protocol 
   family. Values of this type must be cast to struct sockaddr for use with the
   socket interfaces defined in this document. */
#include<netinet/in.h>
/* Para poder usar la funcion memset */
#include<string.h>
/* inet_ntop - convert IPv4 and IPv6 addresses from binary to text form */
#include <arpa/inet.h>

/* Funcion que imprime el error y termina con el programa */
void error(char *msg) {
  perror(msg);
  exit(1);
}

/* Funcion que pone en marcha el servidor. */
int main(int argc, char *argv[]) {
  int sockfd, newsockfd, portno;
  socklen_t size;
  char buffer[1000];
  struct sockaddr_in serv_addr, cli_addr;
  int n;
  char ipstr[256];
  
  if (argc < 2)
    error("Porfavor ingrese un puerto como argumento.");

  /* This call returns a socket descriptor that you can use in later system
     calls or -1 on error.
     El primer argumento corresponde a el protocolo de familia, en este caso
     AF_INET -> IPv4 protocols
     El segundo argumento es el tipo de socket que se desea ocupar, en este caso
     SOCK_STREAM -> Stream Socket 
     El tercer argumento es el tipo de protocolo, si le ponemos 0 estamos 
     seleccionando el protocolo por defecto del sistema para la combinacion de
     protocolo de familia y tipo de socket, que en este caso es TCP transport
     protocol. */
  sockfd = socket(AF_INET, SOCK_STREAM, 0);

  if (sockfd < 0)
    error("Error al intentar abrir el socket.");

  /* The C library function void *memset(void *str, int c, size_t n) copies the
     character c (an unsigned char) to the first n characters of the string 
     pointed to, by the argument str.
     Llenamos de ceros la estructura */
  memset(&serv_addr, 0, sizeof(serv_addr));

  /* Convertimos el numero de puerto dado en el argumento a tipo entero */
  portno = atoi(argv[1]);

  /* Asignamos valores correspondientes a nuestra estructura */
  serv_addr.sin_family = AF_INET;
  serv_addr.sin_addr.s_addr = INADDR_ANY;
  // Network Format (Network Byte Order)
  serv_addr.sin_port = htons(portno);

  /* The bind function assigns a local protocol address to a socket. With the
     Internet protocols, the protocol address is the combination of either a
     32-bit IPv4 address or a 128-bit IPv6 address, along with a 16-bit TCP
     or UDP port number. This function is called by TCP server only.
     This call returns 0 if it successfully binds to the address, otherwise 
     it returns -1 on error.*/
  if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0)
    error("Error en la funcion bind.");

  /* The listen function is called only by a TCP server and it performs two 
     actions −The listen function converts an unconnected socket into a passive
     socket, indicating that the kernel should accept incoming connection
     requests directed to this socket.The second argument to this function 
     specifies the maximum number of connections the kernel should queue for
     this socket. */
  if(listen(sockfd,5) < 0)
    error("Error en la funcion listen.");

  size = sizeof(cli_addr);

  /* The accept function is called by a TCP server to return the next completed
     connection from the front of the completed connection queue. This call
     returns a non-negative descriptor on success, otherwise it returns -1 on
     error. The returned descriptor is assumed to be a client socket descriptor 
     and all read-write operations will be done on this descriptor to
     communicate with the client. */
  newsockfd = accept(sockfd,(struct sockaddr *) &cli_addr, &size);

  if (newsockfd < 0)
    error("Error en la funcion aceptar.");

  /* Pasamos la direccion del cliente a char e imprimimos en el servidor de
     donde viene la conexion */
  struct sockaddr_in *s = (struct sockaddr_in *)&cli_addr;
  inet_ntop(AF_INET, &s->sin_addr, ipstr, sizeof ipstr);
  printf("server: Conexion aceptada desde %s:%d\n", ipstr, portno);

  FILE *fp;
  /* Ciclo en el que leemos del cliente y escribimos al cliente, hasta que el
   cliente escriba EOF en la terminal */
  while(1) {
    memset(buffer, 0, 1000);

    /* Lectura */
    n = read(newsockfd,buffer,1000);
    if(n < 0)
      error("Error al leer del socket.");
      
    /* The popen() function shall execute the command specified by the string
       command. It shall create a pipe between the calling program and the
       executed command, and shall return a pointer to a stream that can be used
       to either read from or write to the pipe. */
    printf("%s",buffer);
    fp = popen(buffer, "r");
    /* The C library function size_t fread(void *ptr, size_t size, size_t nmemb,
       FILE *stream) reads data from the given stream into the array pointed to,
       by ptr. */
    fread(buffer, 1000, 1, fp);

    /* Escritura */
    n = write(newsockfd, buffer, 1000);
    if (n < 0)
      error("Error al escribir en el socket.");

    pclose(fp);
  }
  pclose(fp);  
  return 0;
}
