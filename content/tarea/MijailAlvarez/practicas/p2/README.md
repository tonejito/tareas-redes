# Práctica 2
## Alvarez Cerrillo Mijail Aron
### 310020590

Ejecutamos el makefile con make.

Este programa no saca la salida STDERR y no se hizo ninguna actividad extra.

Iniciamos el programa en local host y puerto 6666
![](https://gitlab.com/mxnmijail/tareas-redes/raw/master/content/tarea/MijailAlvarez/practicas/p2/imagenes/1.png)

Probamos ls
![](https://gitlab.com/mxnmijail/tareas-redes/raw/master/content/tarea/MijailAlvarez/practicas/p2/imagenes/2.png)

Probamos cat
![](https://gitlab.com/mxnmijail/tareas-redes/raw/master/content/tarea/MijailAlvarez/practicas/p2/imagenes/3.png)

EOF
![](https://gitlab.com/mxnmijail/tareas-redes/raw/master/content/tarea/MijailAlvarez/practicas/p2/imagenes/4.png)
