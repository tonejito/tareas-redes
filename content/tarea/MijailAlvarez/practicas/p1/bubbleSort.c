#include<stdlib.h>
#include<stdio.h>

/* Punto extra numero uno de la practica uno.
   Alvarez Cerrillo Mijail Aron
*/

void swap(int *a, int *b) {
  int temporal = *a;
  *a = *b;
  *b = temporal;
}

int main(int argc, char *argv[]) {
  int arrayNum[argc -1];
  int array_size = sizeof(arrayNum)/sizeof(arrayNum[0]);

  // Llenamos nuestro arreglo de enteros con los argumentos
  printf("Arreglo:     ");
  for(int i = 1; i < argc; i ++) {
    arrayNum[i - 1] = atoi(argv[i]);
    printf("%d ", arrayNum[i-1]);
  }

  // Algoritmo bubbleSort
  for(int i = 0; i < array_size; i ++) {
    for (int j = 0; j < array_size - i - 1; j++) {
      if(arrayNum[j] > arrayNum[j + 1])
  	swap(&arrayNum[j], &arrayNum[j+1]);
    }
  }

  // Recorremos el arreglo ordenado
  printf("\n");
  printf("Ordenado:    ");
  for(int i = 0; i < array_size; i++) {
    printf("%d ", arrayNum[i]);
  }
  return 0;
}
